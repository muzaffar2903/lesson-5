// Task 1
// function vowel_count(str1) {
//   let vowel_list = "aeiouAEIOU";
//   let vcount = 0;

//   for (let x = 0; x < str1.length; x++) {
//     if (vowel_list.indexOf(str1[x]) !== -1) {
//       vcount += 1;
//     }
//   }
//   return vcount;
// }
// console.log(vowel_count("The quick brown fox"));

// Task 2
// today = new Date();
// const newYear = new Date(today.getFullYear(), 11, 25);
// if (today.getMonth() == 11 && today.getDate() > 25) {
//   newYear.setFullYear(newYear.getFullYear() + 1);
// }
// const one_day = 1000 * 60 * 60 * 24;
// console.log(
//   `${Math.ceil(
//     (newYear.getTime() - today.getTime()) / one_day
//   )} days left until Christmas!`
// );

// Task 3

let arrSymbol = ["!", "@", "#", "$", 4, "text", "%", "^", "&", "*", "test"];

const rx = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

const checkSymbol = (symbol) => symbol.filter((s) => rx.test(s));

console.log(checkSymbol(arrSymbol));

// Task 4
// program to perform intersection between two arrays using Set
// intersection contains the elements of array1 that are also in array2

// function performIntersection(arr1, arr2) {
//   // converting into Set
//   const setA = new Set(arr1);
//   const setB = new Set(arr2);

//   let intersectionResult = [];

//   for (let i of setB) {
//     if (setA.has(i)) {
//       intersectionResult.push(i);
//     }
//   }

//   return intersectionResult;
// }

// const arr1 = prompt("Enter array1");
// const arr2 = prompt("Enter array2");

// const result = performIntersection(arr1, arr2);
// console.log(result);

// Task 5
// function init() {
//   let count = 5;
//   let counter = setInterval(timer, 1000);
//   function timer() {
//     count = count - 1;
//     if (count == 0) {
//       alert("This is an alert");
//       window.location = "http://www.example.com";
//       return;
//     }
//   }
// }
// // window.onload = init;
// window.onload = setTimeout(function () {
//   alert("This is an alert");
//   window.location = "http://www.example.com";
// }, 4000);

// Task 6
// program to display the sum of natural numbers

// take input from the user
// const number = parseInt(prompt("Enter a positive integer: "));

// let sum = 0,
//   i = 1;

// // looping from i = 1 to number
// while (i <= number) {
//   sum += i;
//   i++;
// }

// console.log("The sum of natural numbers:", sum);

// Task 7
// program to check the number of occurrence of a character

// function countString(str, letter) {
//   let count = 0;

//   // looping through the items
//   for (let i = 0; i < str.length; i++) {
//     // check if the character is at that position
//     if (str.charAt(i) == letter) {
//       count += 1;
//     }
//   }
//   return count;
// }

// // take input from the user
// const string = prompt("Enter a string: ");
// const letterToCheck = prompt("Enter a letter to check: ");

// //passing parameters and calling the function
// const result = countString(string, letterToCheck);

// // displaying the result
// console.log(result);
